from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login


def register_new_user(request):
    form = UserCreationForm()

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()

            if user is not None:
                login(request, user)
                return redirect("home")

    context = {"form": form}
    return render(request, "registration/signup.html", context)
