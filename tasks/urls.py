from django.urls import path
from . import views

urlpatterns = [
    path("create/", views.create_new_task, name="create_task"),
    path("mine/", views.show_user_tasks, name="show_my_tasks"),
    path("<int:pk>/complete/", views.complete_user_task, name="complete_task"),
]
