from django.shortcuts import render
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from .models import Task


@login_required
def create_new_task(request):
    form = TaskForm()

    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", task.project.pk)

    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_user_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)

    context = {"tasks": tasks}

    return render(request, "tasks/list.html", context)


@login_required
def complete_user_task(request, pk):
    task = Task.objects.get(id=pk)
    task.is_completed = True
    task.save()
    return redirect("show_my_tasks")
