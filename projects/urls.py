from django.urls import path
from . import views

urlpatterns = [
    path("", views.view_list_of_projects, name="list_projects"),
    path("<int:pk>/", views.show_project_details, name="show_project"),
    path("create/", views.create_new_project, name="create_project"),
]
