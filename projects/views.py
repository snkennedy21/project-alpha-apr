from django.shortcuts import render
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from django.shortcuts import redirect


@login_required
def view_list_of_projects(request):
    user = request.user
    projects = Project.objects.filter(members=user)

    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project_details(request, pk):
    project = Project.objects.get(id=pk)

    context = {"project": project}

    return render(request, "projects/detail.html", context)


@login_required
def create_new_project(request):
    form = ProjectForm()

    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", project.pk)

    context = {"form": form}
    return render(request, "projects/create.html", context)
